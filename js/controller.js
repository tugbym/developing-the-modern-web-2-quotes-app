angular.module('myApp', [])
    .controller('QuotesController', ['$http',
    
    function ($http) {
        var self = this;
        self.url = 'https://quotesapp.firebaseio.com/Authors.json'
        
        //Get the quotes list from Firebase.
        $http.get(self.url).then(function (data) {
            self.dataArray = data.data;
            
            //Convert the array to an object to enable filtering.
            if (self.dataArray) {
                self.dataArray = Object.keys(data.data).map(function(k) { return data.data[k]; });
            }
            
		    self.quotes = self.dataArray;
            
        //There was an error with the GET request.
        }, function (error) {
            console.error('Error while fetching json data');
        });
        
        self.search = function () {
            self.url2 = 'http://www.stands4.com/services/v2/quotes.php?uid=3663&tokenid=Dw4fIlvSyxhYBXIf&searchtype=AUTHOR&query=' + self.author;
            
            //GET the user's search from the Stands4 API.
            $http.get(self.url2).then(function (data) {
                self.showError = false;
                
                //Convert the XML response to JSON.
                var x2js = new X2JS();
                var json = x2js.xml_str2json(data.data);
                self.authorResults = json.results;
                
                //Found no author, so let's tell the user.
                if (self.authorResults == '') {
                    self.showError = true;
                    self.error = "No results found.";
                }
                
                //For each result, add a unique index to each one.
                angular.forEach(self.authorResults.result, function(data, index){
                    data.index = index;
                });
                
                self.result = self.authorResults.result;
                
            //There was an error getting the data.
            }, function (error) {
            console.error('Error getting the data.');
        });
        }
        
        self.addQuote = function (index) {
            //Grab the quote the user selected based on the index.
            var quoteSelected = self.authorResults.result[index];
            
            //Delete that index, as we don't want it stored in Firebase.
            delete quoteSelected.index;
            
            //POST the selected quote to our quotes list.
            $http.post(self.url, quoteSelected)
                
                //Success! The quote has now been uploaded.
                .success(function(data, status, headers, config) {
                
                //But, we have no way to remember Firebase's randomly generated ID. So, let's grab that and PUT it in along with the quote.
                var quoteUid = data.name;
                quoteSelected.id = quoteUid;
                var updateUrl = 'https://quotesapp.firebaseio.com/Authors/' + quoteUid + '.json';
                $http.put(updateUrl, quoteSelected)
                
                    //Success! We now have a new quote with its unique ID in Firebase.
                    .success(function(data, status, headers, config) {
                    
                    //For each result, add the unique index back in.
                    angular.forEach(self.authorResults.result, function(data, index) {
                        data.index = index;
                    });
                    
                    //GET the newly updated quotes list back out, converting it to an object.
                    $http.get(self.url).then(function (data) {
                        self.dataArray = data.data;
                        self.dataArray = Object.keys(data.data).map(function(k) { return data.data[k]; });
		                self.quotes = self.dataArray;
                        
                    //Error whilst getting the quotes data.
                    }, function (error) {
                        console.error('Error while fetching json data');
                    });
                    
                //Error whilst updating the quote with the unique ID.    
                }).error(function(data, status, headers, config) {
                    console.error('Error while processing unique ID');
                });
            })
            
            //Error whilst posting the new quote.
            .error(function(data, status, headers, config) {
                console.error('Error while posting json data');
            });
        }
        
        self.deleteQuote = function (id) {
            
            //Delete the selected quote.
            var deleteUrl = 'https://quotesapp.firebaseio.com/Authors/' + id + '.json';
            $http.delete(deleteUrl)
            
                //Successful DELETE! So, let's refresh the quotes list.
                .success(function(data, status, headers, config) {
                    $http.get(self.url)
                    
                    //Successful GET. Now, let's convert the array to an object and show the user.
                    .then(function (data) {
                    self.dataArray = data.data;
                    if (self.dataArray) {
                        self.dataArray = Object.keys(data.data).map(function(k) { return data.data[k]; });
                    }
		            self.quotes = self.dataArray;
                        
                    //Error whilst getting the quotes list.
                    }, function (error) {
                        console.error('Error whilst getting json data.');
                    });
            })
            
            //Error whilst deleting the selected quote.
            .error(function(data, status, headers, config) {
                console.error('Error while deleting the quote.');
            });
        }

    }]);